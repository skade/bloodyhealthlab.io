'use strict'
const pages = {
  de: document.getElementById('de'),
  en: document.getElementById('en'),
  impressum: document.getElementById('impressum')
}
const homeIcon = document.getElementById('home-link')

document.getElementById('impressum-link').addEventListener('click', () => show('impressum'))
document.getElementById('en-link').addEventListener('click', () => show('en'))
document.getElementById('de-link').addEventListener('click', () => show('de'))
document.getElementById('home-link').addEventListener('click', () => show('en'))

show('en')

function show(pageId) {
  Object.keys(pages).forEach(id => {
    const shouldShow = id === pageId
    pages[id].style.display = shouldShow ? 'block' : 'none'
  })
  homeIcon.style.display = pageId === 'impressum' ? 'block' : 'none'
  document.documentElement.lang === pageId === 'en' ? 'en' : 'de'
  scroll(0,0)
}

Array.from(document.getElementsByClassName('close'))
  .forEach(button => {
    button.addEventListener('click', () => {
      button.parentElement.style.display = 'none'
    })
  })
